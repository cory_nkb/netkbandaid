﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;

namespace netkSetup
{
    internal class LoggingUtilities
    {
        /// <summary>
        /// Returns the location where we're storing our log files, creating the directory if necasary.
        /// </summary>
        public static string LogDirectory
        {
            get
            {
                var folder = Path.Combine(Directory.GetCurrentDirectory(), "logs");
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                return folder;
            }
        }

        /// <summary>
        /// Returns the full path for the day's log file
        /// </summary>
        public static readonly string LogFile = Path.Combine(LogDirectory, $"{Dns.GetHostName()}-NetKonnectSetup.log");

        /// <summary>
        /// Adds a time stamped error message to the end of the log file
        /// </summary>
        /// <param name="error"></param>
        public static void AppendLog(string error)
        {
            using (var writer = new StreamWriter(LogFile, true))
            {
                writer.WriteLine($"{DateTime.Now.ToLongTimeString()}: {error}");
            }
        }

        /// <summary>
        /// Adds a seperator to the log file for better readability
        /// </summary>
        public static void AddSeperator()
        {
            using (var writer = new StreamWriter(LogFile, true))
            {
                writer.WriteLine();
                for (int i = 0; i < 30; i++)
                {
                    writer.Write("~");
                    if (i == 14)
                    {
                        writer.Write(DateTime.Today.ToString("yyyy-M-d dddd"));
                    }
                }
                writer.WriteLine();
            }
        }
    }
}