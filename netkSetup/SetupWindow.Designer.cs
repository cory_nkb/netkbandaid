﻿namespace netkSetup
{
    partial class SetupWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupWindow));
            this.debugButton = new System.Windows.Forms.Button();
            this.serverAddressTextBox = new System.Windows.Forms.TextBox();
            this.serverAddressLabel = new System.Windows.Forms.Label();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.desktopShortcutCheckBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.auditOnlyCheckBox = new System.Windows.Forms.CheckBox();
            this.logTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // debugButton
            // 
            this.debugButton.Location = new System.Drawing.Point(350, 394);
            this.debugButton.Name = "debugButton";
            this.debugButton.Size = new System.Drawing.Size(84, 42);
            this.debugButton.TabIndex = 0;
            this.debugButton.Text = "Execute";
            this.debugButton.UseVisualStyleBackColor = true;
            this.debugButton.Click += new System.EventHandler(this.ButtonClick);
            // 
            // serverAddressTextBox
            // 
            this.serverAddressTextBox.Location = new System.Drawing.Point(12, 29);
            this.serverAddressTextBox.Name = "serverAddressTextBox";
            this.serverAddressTextBox.Size = new System.Drawing.Size(420, 20);
            this.serverAddressTextBox.TabIndex = 1;
            // 
            // serverAddressLabel
            // 
            this.serverAddressLabel.AutoSize = true;
            this.serverAddressLabel.Location = new System.Drawing.Point(9, 13);
            this.serverAddressLabel.Name = "serverAddressLabel";
            this.serverAddressLabel.Size = new System.Drawing.Size(139, 13);
            this.serverAddressLabel.TabIndex = 2;
            this.serverAddressLabel.Text = "NetKonnect Server Address";
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // desktopShortcutCheckBox
            // 
            this.desktopShortcutCheckBox.AutoSize = true;
            this.desktopShortcutCheckBox.Checked = true;
            this.desktopShortcutCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.desktopShortcutCheckBox.Location = new System.Drawing.Point(12, 55);
            this.desktopShortcutCheckBox.Name = "desktopShortcutCheckBox";
            this.desktopShortcutCheckBox.Size = new System.Drawing.Size(131, 17);
            this.desktopShortcutCheckBox.TabIndex = 3;
            this.desktopShortcutCheckBox.Text = "Add Desktop Shortcut";
            this.desktopShortcutCheckBox.UseVisualStyleBackColor = true;
            this.desktopShortcutCheckBox.CheckStateChanged += new System.EventHandler(this.desktopShortcutCheckBox_CheckStateChanged);
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(12, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(420, 2);
            this.label1.TabIndex = 4;
            // 
            // auditOnlyCheckBox
            // 
            this.auditOnlyCheckBox.AutoSize = true;
            this.auditOnlyCheckBox.Location = new System.Drawing.Point(12, 78);
            this.auditOnlyCheckBox.Name = "auditOnlyCheckBox";
            this.auditOnlyCheckBox.Size = new System.Drawing.Size(74, 17);
            this.auditOnlyCheckBox.TabIndex = 5;
            this.auditOnlyCheckBox.Text = "Audit Only";
            this.auditOnlyCheckBox.UseVisualStyleBackColor = true;
            this.auditOnlyCheckBox.CheckStateChanged += new System.EventHandler(this.auditOnlyCheckBox_CheckStateChanged);
            // 
            // logTextBox
            // 
            this.logTextBox.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logTextBox.Location = new System.Drawing.Point(12, 113);
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.ReadOnly = true;
            this.logTextBox.Size = new System.Drawing.Size(420, 275);
            this.logTextBox.TabIndex = 6;
            this.logTextBox.Text = "";
            // 
            // SetupWindow
            // 
            this.AcceptButton = this.debugButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 448);
            this.Controls.Add(this.logTextBox);
            this.Controls.Add(this.auditOnlyCheckBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.desktopShortcutCheckBox);
            this.Controls.Add(this.serverAddressLabel);
            this.Controls.Add(this.serverAddressTextBox);
            this.Controls.Add(this.debugButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SetupWindow";
            this.Text = "NetKonnect Setup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button debugButton;
        private System.Windows.Forms.TextBox serverAddressTextBox;
        private System.Windows.Forms.Label serverAddressLabel;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.CheckBox desktopShortcutCheckBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox auditOnlyCheckBox;
        private System.Windows.Forms.RichTextBox logTextBox;
    }
}

