using Microsoft.Deployment.WindowsInstaller;
using Microsoft.Win32;
using netkSetup.Properties;
using System;
using System.Collections;
using System.ComponentModel;
using System.DirectoryServices.ActiveDirectory;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;

namespace netkSetup
{
    public class NetKonnServer
    {
        private readonly RegistryUtils _registryUtils = new RegistryUtils();

        /// <summary>
        /// The raw data that the user enters in the textbox
        /// </summary>
        public string RawUrl { get; set; }

        /// <summary>
        /// Our safe url. Can be null, and is tested with NetKonnServer.IsValidUrl
        /// </summary>
        public static string SafeUrl { get; set; }

        private static string MsiFullPath => Path.Combine(Path.GetTempPath(), @"NFPSetup.msi");

        /// <summary>
        /// IP address of the server
        /// </summary>
        public static string IpAddress { get; set; }

        /// <summary>
        /// Hostname of the server
        /// </summary>
        public static string HostName { get; set; }

        /// <summary>
        /// Test if the DNS resolution failed
        /// </summary>
        public static bool DnsResolutionSuccessful { get; set; }

        /// <summary>
        /// Tests whether the user data is either a valid url, or can be made a valid url
        /// </summary>
        public bool IsValidUrl
        {
            get
            {
                MakeSafeUrl();
                return !string.IsNullOrEmpty(SafeUrl);
            }
        }

        /// <summary>
        /// Validates that the required ports are open on the server.
        /// </summary>
        public bool PortsOpen => PortCheckSuccessful();

        /// <summary>
        /// Tests whether the required ports are accessable on the server.
        /// </summary>
        /// <returns></returns>
        private static bool PortCheckSuccessful()
        {
            // If the DNS resolution failed, return false
            if (string.IsNullOrEmpty(IpAddress)) return false;

            try
            {
                var tcpClient = new TcpClient();

                tcpClient.Connect(IpAddress, 80);
                tcpClient.Close();

                tcpClient = new TcpClient();

                tcpClient.Connect(IpAddress, 9300);
                tcpClient.Close();

                return true;
            }
            catch (Exception e)
            {
                var rawMessage = e.Message;
                var messageArray = rawMessage.Split(':');
                var port = messageArray[1];
                LoggingUtilities.AppendLog($"Port {port} closed on server.");
                return false;
            }
        }

        /// <summary>
        /// Tries to turn the RawUrl into a valid url.
        /// </summary>
        public void MakeSafeUrl()
        {
            var url = RawUrl;
            if (!url.Contains(@"http://"))
            {
                url = $"http://{RawUrl}";
            }

            Uri serverAddress;
            var addressIsUrl = Uri.TryCreate(url, UriKind.Absolute, out serverAddress) && serverAddress.Scheme == Uri.UriSchemeHttp;
            if (addressIsUrl)
            {
                SafeUrl = serverAddress.ToString();
                HostName = serverAddress.DnsSafeHost.ToLower();
                if (serverAddress.HostNameType == UriHostNameType.IPv4)
                {
                    IpAddress = HostName;
                }
                if (serverAddress.HostNameType != UriHostNameType.Dns) return;
                try
                {
                    var netKonnHostEntry = Dns.GetHostEntry(HostName);
                    if (netKonnHostEntry.AddressList.Length > 0)
                    {
                        IpAddress = netKonnHostEntry.AddressList[0].ToString();
                    }
                    DnsResolutionSuccessful = true;
                }
                catch (Exception)
                {
                    LoggingUtilities.AppendLog($"Could not resolve \"{RawUrl}\" to an IP adress.");
                    DnsResolutionSuccessful = false;
                    IpAddress = null;
                    return;
                }
                return;
            }
            LoggingUtilities.AppendLog($"No safe URL could be created from \"{RawUrl}\".");
            SafeUrl = null;
        }

        /// <summary>
        /// Runs the unpacked MSI file
        /// </summary>
        public void RunPermissionsSetupMsi()
        {
            if (PermissionsSetupHasRun) return;

            LoggingUtilities.AppendLog("Permission setup MSI has not been run, running.");
            if (Globals.AuditOnly) return; /*Exit if we're in a dry run*/
            var msiLogPath = Path.Combine(LoggingUtilities.LogDirectory, @"NFPSetup.log");
            UnpackMsi();
            Installer.SetInternalUI(InstallUIOptions.Silent);
            Installer.EnableLog(InstallLogModes.Verbose, msiLogPath);
            Installer.InstallProduct(MsiFullPath, "");
        }

        /// <summary>
        /// Determines whether the permissions setup MSI has been run
        /// </summary>
        public bool PermissionsSetupHasRun
        {
            get
            {
                var programFilesDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
                var permissionsDirectory = Path.Combine(programFilesDirectory,
                    @"NIHON KOHDEN\.Net Framework Permission for NetKonnect");
                return Directory.Exists(permissionsDirectory);
            }
        }

        public RegistryUtils RegistryUtils => _registryUtils;

        /// <summary>
        /// Unpacks the .Net permissions MSI resource
        /// </summary>
        private static void UnpackMsi()
        {
            using (var binaryWriter = new BinaryWriter(File.Open(MsiFullPath, FileMode.OpenOrCreate)))
            {
                binaryWriter.Write(Resources.NFPSetup);
            }
        }

        /// <summary>
        /// Adds a shortcut pointing to the NetKonnect login page to the desktop
        /// </summary>
        public void AddDesktopShortcut()
        {
            if (SafeUrl == null)
            {
                LoggingUtilities.AppendLog("Cannot add shortcut - no safe URL could be created.");
                return;
            }
            var dektopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var shortcutPath = Path.Combine(dektopDirectory, "NetKonnect.url");
            var baseUrl = new Uri(SafeUrl);
            var netKonnectLoginUrl = new Uri(baseUrl, "netkonnect/login.aspx");

            using (var writer = new StreamWriter(shortcutPath))
            {
                writer.WriteLine("[InternetShortcut]");
                writer.WriteLine($"URL={netKonnectLoginUrl}");
                writer.Flush();
            }
        }
    }
}