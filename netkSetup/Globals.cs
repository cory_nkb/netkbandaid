namespace netkSetup
{
    public class Globals
    {
        public static bool AddDesktopShortcut { get; set; } = true;
        public static bool AuditOnly { get; set; }
    }
}