﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace netkSetup
{
    public partial class SetupWindow : Form
    {
        public SetupWindow()
        {
            InitializeComponent();
        }

        public NetKonnServer NetKonn = new NetKonnServer();

        private void ButtonClick(object sender, EventArgs e)
        {
            ToggleForm(false);

            backgroundWorker.RunWorkerAsync();
        }

        private void ToggleForm(bool enabledState)
        {
            foreach (Control control in Controls)
            {
                control.Enabled = enabledState;
            }
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            LoggingUtilities.AddSeperator();
            if (Globals.AuditOnly) LoggingUtilities.AppendLog(@"***Dry run - no changes will be made***");
            NetKonn.RawUrl = serverAddressTextBox.Text.Trim();
            NetKonn.MakeSafeUrl();
            if (!NetKonn.PortsOpen) return;
            NetKonn.RegistryUtils.VerifyTrustedSites();
            NetKonn.RegistryUtils.AddIeHostingDword();
            NetKonn.RunPermissionsSetupMsi();
            if (Globals.AddDesktopShortcut) NetKonn.AddDesktopShortcut();
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            logTextBox.Text = File.ReadAllText(LoggingUtilities.LogFile);
            ToggleForm(true);
        }

        private void desktopShortcutCheckBox_CheckStateChanged(object sender, EventArgs e)
        {
            var checkBox = (CheckBox)sender;
            Globals.AddDesktopShortcut = checkBox.Checked;
        }

        private void auditOnlyCheckBox_CheckStateChanged(object sender, EventArgs e)
        {
            var checkBox = (CheckBox)sender;
            Globals.AuditOnly = checkBox.Checked;
            desktopShortcutCheckBox.Checked = !checkBox.Checked;
            desktopShortcutCheckBox.Enabled = !checkBox.Checked;
        }
    }
}