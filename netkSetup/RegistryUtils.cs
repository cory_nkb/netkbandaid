using Microsoft.Win32;
using System;
using System.Collections;
using System.DirectoryServices.ActiveDirectory;
using System.IO;
using System.Linq;

namespace netkSetup
{
    public class RegistryUtils
    {
        private static readonly string DomainKey = Path.Combine(ManagedByDomain ? GroupPolicyZoneMapRegKey : ZoneMapRegKey, "Domains");
        private static readonly string RangesKey = Path.Combine(ManagedByDomain ? GroupPolicyZoneMapRegKey : ZoneMapRegKey, "Ranges");
        private const string ZoneMapRegKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\";
        private const string GroupPolicyZoneMapRegKey = @"SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\";

        private const string ZoneMapKey =
            @"SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMapRegKey";

        /// <summary>
        /// Registry location of trusted sited
        /// </summary>
        private static RegistryKey TrustedSites => Registry.CurrentUser.OpenSubKey(NetKonnServer.HostName != NetKonnServer.IpAddress ? DomainKey : RangesKey);

        /// <summary>
        /// Tests whether a given domain name is in trusted sites
        /// </summary>
        public bool InTrustedSites
        {
            get
            {
                if (NetKonnServer.IpAddress != NetKonnServer.HostName)
                    return TrustedSites != null && TrustedSites.GetSubKeyNames().Contains(NetKonnServer.HostName);
                if (TrustedSites != null)
                    foreach (var subKey in TrustedSites.GetSubKeyNames())
                    {
                        var rangesSubKeyPath = Path.Combine(RangesKey, subKey);
                        var rangesSubKey = Registry.CurrentUser.OpenSubKey(rangesSubKeyPath);
                        if (rangesSubKey == null) continue;
                        foreach (var valueName in rangesSubKey.GetValueNames())
                        {
                            var value = Registry.GetValue(rangesSubKey.ToString(), valueName, null);
                            if (value.ToString() == NetKonnServer.IpAddress) return true;
                        }
                    }

                return false;
            }
        }

        /// <summary>
        /// The decimal value of the Zone 2 "flags" DWORD
        /// </summary>
        private static int HttpsValueInt
        {
            get
            {
                // The value of the flags dword, as a string
                var rawValue = HttpsKey?.GetValue("flags");

                // Convert the value to an int
                var valueInt = Convert.ToInt32((object)rawValue);
                return valueInt;
            }
        }

        /// <summary>
        /// The registry key where the HTTPS value is stored
        /// </summary>
        private static RegistryKey HttpsKey
        {
            get
            {
                // The string indicating the location for the security zone
                var httpsKeyString = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\2";

                // The registry key representing the zecurity zone
                var httpsKey = Registry.CurrentUser.OpenSubKey(httpsKeyString, true);
                return httpsKey;
            }
        }

        /// <summary>
        /// Returns the current domain of the logged in user
        /// </summary>
        public static Domain CurrentDomain
        {
            get
            {
                try
                {
                    var domain = Domain.GetCurrentDomain();
                    return domain;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Determines if the client PC is manged by a domain
        /// </summary>
        public static bool ManagedByDomain => CurrentDomain != null;

        /// <summary>
        /// Adds the URL to trusted sites
        /// </summary>
        public static void AddTrustedSites()
        {
            if (NetKonnServer.SafeUrl == null) return;
            if (ManagedByDomain)
            {
                LoggingUtilities.AppendLog($"PC Is currently on domain: {CurrentDomain}. Please contact your IT department to have {NetKonnServer.SafeUrl} added to trusted sites.");
                return;
            }
            LoggingUtilities.AppendLog("URL not in trusted sites, adding.");
            if (Globals.AuditOnly) return; /*Exit if we're doing a dry run*/
            if (NetKonnServer.DnsResolutionSuccessful)
            {
                AddDomain();
                return;
            }
            AddRange();
        }

        /// <summary>
        /// Tests if HTTPS is required for trusted sites, and if it is disables the requirement.
        /// </summary>
        private static void DisableHttpsRegistry()
        {
            if (!RequireHttpsRegistrySet()) return;
            var newDword = HttpsValueInt - 4;
            HttpsKey?.SetValue("flags", newDword, RegistryValueKind.DWord);
        }

        /// <summary>
        /// Parses the HttpsValueInt and determines whether the "Require HTTPS" flags has been set
        /// </summary>
        /// <returns></returns>
        private static bool RequireHttpsRegistrySet()
        {
            // Convert the value to a bit array
            var valueBitRaw = new BitArray(new[] { HttpsValueInt });

            // Convert the bit array to an array of ints
            var valueBit = valueBitRaw.Cast<bool>().Select(bit => bit ? 1 : 0).ToArray();

            // Get the value of the third bit, and convert it to a boolean
            var requireHttps = Convert.ToBoolean(valueBit[2]);
            return requireHttps;
        }

        /// <summary>
        /// Add an IP to trusted sites, in the Ranges subkey
        /// </summary>
        private static void AddRange()
        {
            var rangeKeyString = Path.Combine(RangesKey, GetNextAvailableKey());
            Registry.CurrentUser.CreateSubKey(rangeKeyString);

            var rangeKey = Registry.CurrentUser.OpenSubKey(rangeKeyString, true);
            rangeKey?.SetValue(@":Range", NetKonnServer.IpAddress, RegistryValueKind.String);
            rangeKey?.SetValue(@"http", 2, RegistryValueKind.DWord);
        }

        /// <summary>
        /// Gets the next available subkey in Ranges
        /// </summary>
        /// <returns></returns>
        private static string GetNextAvailableKey()
        {
            var availableKey = "";

            try
            {
                var lastKey = TrustedSites.GetSubKeyNames().Last();
                var splitKey = lastKey.Split(new string[] { "Range" }, StringSplitOptions.None);
                var lastNum = Convert.ToInt32(splitKey.Last());
                var availableNum = lastNum + 1;
                availableKey = $"Range{availableNum}";
            }
            catch (Exception)
            {
                availableKey = @"Range1";
            }

            return availableKey;
        }

        /// <summary>
        /// Add a trusted site, via the Domains subkey
        /// </summary>
        private static void AddDomain()
        {
            var domainKeyString = Path.Combine(DomainKey, NetKonnServer.HostName);
            Registry.CurrentUser.CreateSubKey(domainKeyString);
            var domainKey = Registry.CurrentUser.OpenSubKey(domainKeyString, true);
            domainKey?.SetValue(@"http", 2, RegistryValueKind.DWord);
        }

        /// <summary>
        /// Verifies that the URL is in trusted sites, adding the registry entry if needed
        /// </summary>
        public void VerifyTrustedSites()
        {
            if (RequireHttpsRegistrySet() && !ManagedByDomain)
            {
                LoggingUtilities.AppendLog("Trusted sites requires HTTPS, disabling requirement.");
                if (!Globals.AuditOnly && !ManagedByDomain) DisableHttpsRegistry(); /*Only make changes if we're not in a dry run, and not on a domain*/
            }
            if (InTrustedSites) return;
            AddTrustedSites();
        }

        /// <summary>
        /// Add the EnableIEHosting dword
        /// </summary>
        public void AddIeHostingDword()
        {
            var ieHostingKey64 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\MICROSOFT\.NETFramework", true);
            var ieHostingKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\MICROSOFT\.NETFramework", true);

            if (ieHostingKey64 != null && ieHostingKey64.GetValue("EnableIEHosting") == null)
            {
                LoggingUtilities.AppendLog("Wow6432Node EnableIEHosting DWORD not set, setting.");

                if (!Globals.AuditOnly) /*Only continue if we're not in a dry run*/
                    ieHostingKey64.SetValue("EnableIEHosting", 1, RegistryValueKind.DWord);
            }

            if (ieHostingKey != null && ieHostingKey.GetValue("EnableIEHosting") == null)
            {
                LoggingUtilities.AppendLog("EnableIEHosting DWORD not set, setting.");

                if (!Globals.AuditOnly) /*Only continue if we're not in a dry run*/
                    ieHostingKey.SetValue("EnableIEHosting", 1, RegistryValueKind.DWord);
            }
        }
    }
}